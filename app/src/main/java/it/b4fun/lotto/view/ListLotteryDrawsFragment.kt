package it.b4fun.lotto.view.adapter

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.fragment.app.Fragment
import com.gun.diecielotto.view.adapter.ListLotteryDrawsAdapter
import it.b4fun.lotto.LottoActivity
import it.b4fun.lotto.R
import it.b4fun.lotto.db.DbManager

class ListLotteryDrawsFragment: Fragment() {
    val TAG = "ListLotteryDrawsFragment"
    // lateinit var listViewGames: ListView
    lateinit var rootView: View
    // lateinit var gameDataAdapter: ListLotteryDrawsAdapter

    override fun onCreateView(  inflater: LayoutInflater,
                                container: ViewGroup?,
                                savedInstanceState: Bundle?): View? {
        //super.onCreateView(inflater,container,savedInstanceState)
        Log.d(TAG, "- onCreateView -")
        rootView = inflater.inflate(R.layout.list_lottery_draws_fragment, container, false)
        rootView.invalidate()
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Log.d(TAG, "onActivityCreated - ")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.d(TAG, "onSaveInstanceState - ")
    }
}