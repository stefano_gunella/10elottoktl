package com.gun.diecielotto.view.adapter

import android.content.Context
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.gun.diecielotto.util.GamesUtil
import it.b4fun.lotto.LottoApplication
import it.b4fun.lotto.R
import it.b4fun.lotto.R.color.white
import it.b4fun.lotto.db.DbManager
import it.b4fun.lotto.db.LottoDataItem

class ListLotteryDrawsAdapter(context: Context, listLotteryDraws: List<LottoDataItem>) : ArrayAdapter<LottoDataItem?>(
        context, R.layout.list_lottery_draws_fragment, listLotteryDraws
    ) {
    val TAG = "ListLotteryDrawsAdapter"
    private var db: DbManager? = null
    private var listLotteryDraws: List<LottoDataItem> = listLotteryDraws

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        Log.d(TAG, "getView:")
        val la = context.applicationContext as LottoApplication
        val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val rowView: View = inflater.inflate(R.layout.lottery_draw_layout, parent, false)
        if (listLotteryDraws.size > 0) {
            val item: LottoDataItem = listLotteryDraws[position]
            Log.d(TAG, "item:$item")
            val textData: TextView = rowView.findViewById<View>(R.id.data_estrazione) as TextView
            val textProgressivo: TextView = rowView.findViewById<View>(R.id.progressivo_estrazione) as TextView
            textData.setText(item.dateUI)
            textProgressivo.setText("Progressivo #"+item.progressivoGiornaliero)
            val gu: GamesUtil = GamesUtil.getInstance(context)
            val layoutEstrazioni: LinearLayout = rowView.findViewById<View>(R.id.lista_numeri_estratti) as LinearLayout
            for (numero in item.numeriEstratti) {
                val tv = TextView(rowView.context)
                // LayoutParams tlp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                val tlp: ViewGroup.LayoutParams = ViewGroup.LayoutParams(gu.dpToPx(27), gu.dpToPx(27))
                tv.also {
                    it.setGravity(Gravity.CENTER)
                    it.setLayoutParams(tlp)
                    it.setBackgroundResource(R.drawable.blue_ball)
                    it.setTextColor(ContextCompat.getColor(context, R.color.white))
                    //Log.d(TAG, "numero:$numero")
                    it.text = "$numero"
                }
                layoutEstrazioni.addView(tv)
            }
            val checkBox: CheckBox = rowView.findViewById<View>(R.id.chk_draw) as CheckBox
            checkBox.isChecked = la.isCheckedItem[position]
            checkBox.setOnCheckedChangeListener{
                _, // chkView,
                isChecked -> Log.d(TAG, "checked:${checkBox.isChecked()}")
                            // isCheckedItem.set(position,isChecked)
                            la.isCheckedItem[position] = isChecked
                            (context.applicationContext as LottoApplication).updateListNumbers(isChecked,item)

            }
        }
        return rowView
    }

    override fun getItem(position: Int): LottoDataItem? {
        var item = listLotteryDraws.get(position)
        Log.d(TAG, "getItem item:${item._id}")
        return item
    }
}