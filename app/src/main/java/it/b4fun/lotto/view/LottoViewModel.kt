package it.b4fun.lotto.view

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import it.b4fun.lotto.LottoApplication
import it.b4fun.lotto.util.DateUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LottoViewModel(application: Application) : AndroidViewModel(application) {
// class LottoViewModel : AndroidViewModel(){
    val TAG: String = "LottoViewModel"
    private val mApplication: Application? = application

    fun loadAllLotteryDraws() {
        // Create a new coroutine to move the execution off the UI thread
        Log.d(TAG, "loadAllLotteryDraws")
        viewModelScope.launch(Dispatchers.IO) {
            var la = mApplication as LottoApplication
            la.doWebLoadDataTask(DateUtils.getToday(), null)
        }
    }

    override fun onCleared() {
        super.onCleared()
        Log.d(TAG, "LottoViewModel")
    }
}