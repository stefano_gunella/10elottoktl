package it.b4fun.lotto.engine
import android.content.Context
import android.util.Log
import com.gun.diecielotto.util.GamesUtil
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.android.*
import io.ktor.client.network.sockets.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.serialization.gson.*
import it.b4fun.lotto.LottoApplication
import it.b4fun.lotto.db.LottoDataItem
import kotlinx.serialization.*
import java.text.SimpleDateFormat
import java.util.*

class PostTask(context: Context){
    val TAG = "PostTask"
    lateinit var context: Context
    lateinit var gu: GamesUtil
    //val MAX_RETRY = 3
    var progressivoGiornaliero = 0
    var la: LottoApplication

    // {"data": "20220130", "progressivoGiornaliero": "1"}
    @Serializable
    data class LottoRequest(
        val data: String,
        val progressivoGiornaliero: Int)

    @Serializable
    data class LottoResponse(
        val data: Long,                         // "data":1650988499000,
        val progressivoGiornaliero: Int,        // "progressivoGiornaliero":215
        val massimoProgressivoGiornaliero:Int,  // "massimoProgressivoGiornaliero":null,
        val numeriEstratti: IntArray,           // "numeriEstratti":["1","5","11","19","27","33","36","37","38","39","42","47","51","56","57","64","68","74","81","85"],
        val numeriEstrattiOvertime : IntArray,  // "numeriEstrattiOvertime":["2","3","4","7","16","26","28","30","44","50","53","58","59","84","88"],
        val numeroSpeciale : Int,               // "numeroSpeciale":85,
        val doppioNumeroSpeciale : Int          // "doppioNumeroSpeciale":33
    )

    init{
        this.progressivoGiornaliero = progressivoGiornaliero
        this.context = context;
        this.gu = GamesUtil.getInstance(context)
        this.la = context.applicationContext as LottoApplication
    }

    suspend fun getExtraction(date:Date,progressivoGiornaliero:Int) : LottoDataItem? {
        val dateFormat = SimpleDateFormat("yyyyMMdd")
        val s_data = dateFormat.format(date) // "20160129"
        var ld : LottoDataItem?
        /*
        ============================================================
        curl -X POST -H "Content-Type: application/json" \
        -d '{"data": "20220130", "progressivoGiornaliero": "1"}' \
        https://www.lotto-italia.it/del/estrazioni-e-vincite/10-e-lotto-estrazioni-ogni-5.json
        ============================================================
        {"data":1643497499000,
         "progressivoGiornaliero":1,
         "massimoProgressivoGiornaliero":null,
         "numeriEstratti":["4","6","11","18","24","31","34","38","46","49","51","52","55","62","68","72","76","80","81","87"],
         "numeriEstrattiOvertime":["12","13","14","15","19","20","22","45","58","73","74","75","79","84","88"],
         "numeroSpeciale":6,
         "doppioNumeroSpeciale":87}
        ============================================================
       */
        val httpClient: HttpClient = HttpClient(Android){
            install(ContentNegotiation) {
                gson()
            }
            /*
            engine {
               // this: AndroidEngineConfig
               connectTimeout = 10000
               socketTimeout = 10000
               //proxy = Proxy(Proxy.Type.HTTP, InetSocketAddress("localhost", 8080))
            }
            */
        }

        try {
            Log.d(TAG, "...post:$s_data progressivoGiornaliero:$progressivoGiornaliero")
            val url_10_lotto: String = "https://www.lotto-italia.it/del/estrazioni-e-vincite/10-e-lotto-estrazioni-ogni-5.json"
            val response: LottoResponse = httpClient.post(url_10_lotto) {
                /*
                headers {
                    append(HttpHeaders.Accept, "application/json")
                    append(HttpHeaders.ContentType, "application/json")
                    append(HttpHeaders.ContentEncoding, "application/json")
                    append(HttpHeaders.UserAgent, "ciambawamba client")
                }
                */
                contentType(ContentType.Application.Json)
                setBody(LottoRequest(s_data, progressivoGiornaliero))
            }.body()

            // FIXME:inserire nella lista da visualizzare
            var la: LottoApplication = context.applicationContext as LottoApplication
            ld = gu.t_LottoResponse2LottoDataItem(response)
            Log.d(TAG, "...item:" + ld)
            return ld
            // la.db.insertLotteryDraw(ld)
        }
        catch (e: ConnectTimeoutException) {
            Log.d(TAG,progressivoGiornaliero.toString() + ":ConnectTimeoutException:" + e.toString()
            )
        }
        catch (e: Exception) {
            e.printStackTrace()
            val message = "ERROR - POST_TASK data:" + s_data + " Progressivo gg:" + progressivoGiornaliero + " - " + e.message
            this.la.showMessage(message)
            Log.d(TAG, message)
        }
        return null
    }
}

