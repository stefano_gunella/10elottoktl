package it.b4fun.lotto.service

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.HandlerThread
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import it.b4fun.lotto.LottoApplication
import it.b4fun.lotto.util.DateUtils
import java.util.*

class GetLotteryDrawsForegroundService : Service() {
    val TAG : String = "GetLotteryDrawsForegroundService"
    lateinit var handlerThread : HandlerThread
    lateinit var handler:Handler
    private var alarmMgr: AlarmManager? = null
    private lateinit var alarmIntent: Intent
    var index:Int = 0
    var isRunning :Boolean = false

    override fun onCreate() {
        super.onCreate()
        // Start the foreground service immediately.
        startForeground(System.currentTimeMillis().toInt(), getNotification(this))
        Log.d(TAG, "onCreate")
        handlerThread = HandlerThread("Thread Principale")
        with(handlerThread) {
            setDaemon(true)
            this.start()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand")
        if(isRunning){
            Log.d(TAG, "Service is running!")
            return START_STICKY
        }
        Toast.makeText(applicationContext, "Service started.", Toast.LENGTH_SHORT).show()
        handler = Handler(handlerThread.looper)

        // Every other call is up to you. You can update the location,
        // do whatever you want after this part.

        // Sample code (which should call handler.postDelayed()
        // in the function as well to create the repetitive task.)
        handler.post( object:Runnable {
                        override fun run() {
                            index = DateUtils.getLastIndexNow()
                            var today: Date = DateUtils.getToday_30secondsAgo()
                            Log.d(TAG, "start PostTask:" + index)
                            val lottoApplication: LottoApplication = application as LottoApplication
                            lottoApplication.doWebTask(today ,index)
                            handler.postDelayed(this,DateUtils.getPostDelayTime())
                        }
                    }
        );
        Log.d(TAG, "onStartCommand END")
        isRunning = true
        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        // TODO("Not yet implemented")
        return null
    }

    private fun getNotification(context: Context) : Notification{
        val CHANNEL_ID = "my_channel_01"
        val channel = NotificationChannel(
            CHANNEL_ID,
            "Channel human readable title",
            NotificationManager.IMPORTANCE_DEFAULT
        )

        (ContextCompat.getSystemService(context, NotificationManager::class.java) as NotificationManager).createNotificationChannel(channel)

        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("")
            .setContentText("").build()
        return notification
    }
}
