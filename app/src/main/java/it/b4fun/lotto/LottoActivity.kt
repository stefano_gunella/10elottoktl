package it.b4fun.lotto

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log

import android.view.View
import android.widget.*
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
// import androidx.activity.viewModels
// import androidx.core.content.ContextCompat
import com.gun.diecielotto.view.adapter.ListLotteryDrawsAdapter
import it.b4fun.lotto.R.*
import it.b4fun.lotto.db.LottoDataItem
import it.b4fun.lotto.service.GetLotteryDrawsForegroundService
import it.b4fun.lotto.util.DateUtils
import it.b4fun.lotto.view.LottoViewModel
import kotlinx.coroutines.*

class LottoActivity : AppCompatActivity() {
    private val TAG : String = "LottoActivity"
    private var filter = IntentFilter()
    lateinit var la : LottoApplication
    lateinit var listLotteryView: ListView
    lateinit var gridAreaView: GridLayout
    var listLotteryDraws : List<LottoDataItem> = emptyList<LottoDataItem>()
    lateinit var updateUIBroadcastReceiver : BroadcastReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.lotto_main)
        filter.addAction("it.b4fun.lotto.updateUI");
        updateUIBroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                Log.d(TAG, "UPDATE UI and RighArea!!!")
                updateUI(intent.getLongExtra("index",0),
                        intent.getBooleanExtra("rightArea",false)
                        )
            }
        }
        registerReceiver(updateUIBroadcastReceiver, filter)
        la = applicationContext as LottoApplication
        initRightAreaView()

        Log.d(TAG, "onCreate")
    }

    override fun onPostResume() {
        super.onPostResume()
        initListView()
        //initRightAreaView()
        Log.d(TAG, "onPostResume")
    }

    fun initListView() {
        listLotteryDraws = la.db.getListDataDrawsAtDate(DateUtils.getToday())!!
        listLotteryView = this.findViewById<ListView>(R.id.list_lottery_view)
        listLotteryView.let {
            it.adapter = ListLotteryDrawsAdapter(applicationContext, listLotteryDraws)
            // it.choiceMode = ListView.CHOICE_MODE_MULTIPLE;
        }
        Log.d(TAG, "initListView")
    }

    fun initRightAreaView() {
        gridAreaView = this.findViewById<GridLayout>(id.gridLayout)
        for (index in 1..90) {
            val view = this.layoutInflater.inflate(layout.item_number_draw, null)
            val number = view.findViewById<TextView>(id.number)
            val count = view.findViewById<TextView>(id.count)
            // val number = TextView(this)
            // LayoutParams tlp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            // val tlp: ViewGroup.LayoutParams = ViewGroup.LayoutParams(GamesUtil.getInstance(this).dpToPx(30), GamesUtil.getInstance(this).dpToPx(30))
            view.tag = "$index"
            number.text = "$index"
            number.setTextColor(ContextCompat.getColor(this,R.color.white))
            count.text = "${la.numbersDrawn[index-1]}"
            gridAreaView.addView(view)
        }
        Log.d(TAG, "initRightAreaView")
    }

    fun updateRightAreaView() {
        var max_ext = 0
        gridAreaView = this.findViewById<GridLayout>(R.id.gridLayout)
        la.numbersDrawn.forEachIndexed { index, n ->
            val view = gridAreaView.findViewWithTag<LinearLayout>("${index+1}")
            val number = view.findViewById<TextView>(id.number)
            val count = view.findViewById<TextView>(id.count)
            //val tv = rightAreaView.findViewWithTag<TextView>("${index+1}")
            //val count = view.findViewById<TextView>(id.count)
            //Log.d(TAG, "trovata :${tv?.text}")
            // LayoutParams tlp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            // val tlp: ViewGroup.LayoutParams = ViewGroup.LayoutParams(GamesUtil.getInstance(this).dpToPx(30), GamesUtil.getInstance(this).dpToPx(30))
            if (n>0) {
                number.setBackgroundResource(R.drawable.red_ball)
                count.text = "${la.numbersDrawn[index]}"
            }
            else{
                number.setBackgroundResource(R.drawable.blue_ball)
                count.text = "0"
            }
            max_ext = if (max_ext < la.numbersDrawn[index]) la.numbersDrawn[index] else max_ext
        }
        la.numbersDrawn.forEachIndexed { index, n ->
            val view = gridAreaView.findViewWithTag<LinearLayout>("${index+1}")
            val count = view.findViewById<TextView>(id.count)
            if (la.numbersDrawn[index]>(max_ext-2)) {
                // number.setBackgroundResource(R.drawable.green_ball)
                count.text = "${la.numbersDrawn[index]}"
                count.setBackgroundColor(resources.getColor(R.color.green))
            }
            else{
                count.setBackgroundColor(resources.getColor(R.color.white))
            }

        }
        Log.d(TAG, "updateRightAreaView")
    }

    fun onClickResetRightArea(view: View){
        Log.d(TAG, "onClickResetRightArea...")
        la.numbersDrawn.forEachIndexed { index, item -> la.numbersDrawn[index] = 0 }
        la.isCheckedItem.forEachIndexed { index, item -> la.isCheckedItem[index] = false }
        (applicationContext as LottoApplication).updateListNumbers(true,null)
        updateRightAreaView()
    }

    fun onClickStartService(view: View){
        Log.d(TAG, "onClickStartService...")
        val lotteryService = GetLotteryDrawsForegroundService::class.java
        var intentService = Intent(this, lotteryService) // Build the intent for the service
        startForegroundService(intentService)
    }

    /**
     * Dovrebbe caricare con un task asincrono...invece il pulsante resta "premuto" fino al compimento dell'operazione...
     */
    fun onClickLoadAllLotteryDraws(view: View) {
        Log.d(TAG, "onClickLoadAllLotteryDraws...")
        val lottoViewModel = LottoViewModel(this.application) // by viewModels()
        lottoViewModel.loadAllLotteryDraws()
        Log.d(TAG, "END onClickLoadAllLotteryDraws...")
    }

    fun updateUI(id: Long, rightArea:Boolean){
        Log.d(TAG, "updateUI:${id} , rightArea:${rightArea}")
        if (!this.window.decorView.rootView.isShown){
            Log.d(TAG, "Activity is not SHOWED")
            return // do nothing
        }
        if(id > 0) {
            initListView()
            val item = la.db.getLotteryDrawById(id)
            Log.d(TAG, "ITEM:$item")
            listLotteryDraws.plus(item)
            listLotteryView.deferNotifyDataSetChanged();
        }
        if(id.equals(0L) && !rightArea) {
            Log.d(TAG, "LAD ALL DATA")
            initListView()
            listLotteryView.deferNotifyDataSetChanged();
        }
        if(rightArea) {
            updateRightAreaView()
        }
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy")
        super.onDestroy()
        unregisterReceiver(updateUIBroadcastReceiver)
    }

    fun onCheckboxClicked(view: View) {
        var item = view.findViewById<TextView>(R.id.progressivo_estrazione)
        Log.d(TAG, "onCheckboxClicked:${item.text}")
    }
}
