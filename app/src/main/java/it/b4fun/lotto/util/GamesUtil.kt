package com.gun.diecielotto.util

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import it.b4fun.lotto.db.LottoDataItem
import it.b4fun.lotto.engine.PostTask
import it.b4fun.lotto.util.DateUtils
import java.util.*

class GamesUtil{
    private val TAG = "GamesUtil"
    lateinit var context: Context
    var preferences: SharedPreferences? = null

    fun calcolaCombinazioni(datiPartenza: IntArray, elementi: Int) {
        val combinazione = IntArray(elementi)
        //_calcolaCombinazioniR(datiPartenza, combinazione, elementi, 0, 0)
    }

    fun t_LottoResponse2LottoDataItem(lotto: PostTask.LottoResponse): LottoDataItem {
        val res = LottoDataItem()
        res._id = lotto.data
        res.data = DateUtils.getDate(lotto.data)
        res.numeriEstratti = lotto.numeriEstratti
        res.progressivoGiornaliero = lotto.progressivoGiornaliero
        Log.d(TAG,"LottoResponse:$res")
        return res
    }

    /*
    // Metodo ricorsivo per il calcolo delle combinazioni
    private fun _calcolaCombinazioniR(  datiPartenza: IntArray, combinazione: IntArray,
                                        elementi: Int, livello: Int, indicePartenza: Int
    ) {
        if (livello == elementi) {
            val g: GiocateLottoDataItem? = getInstance(context)!!.createGame(combinazione)
            DbManager.getInstance(context).saveGiocate(g)
            return
        }
        for (i in indicePartenza..datiPartenza.size - (elementi - livello)) {
            combinazione[livello] = datiPartenza[i]
            _calcolaCombinazioniR(datiPartenza, combinazione, elementi, livello + 1, i + 1)
        }
    }

    private fun _checkVincita(indovinati: Int, giocata: GiocateLottoDataItem) {
        var vincita = 0
        val numeriGiocatiEffettivi: Int = giocata.getTotaleNumeriGiocati()
        Log.d(TAG, "Giocati:$numeriGiocatiEffettivi - Indovinati:$indovinati")
        when (numeriGiocatiEffettivi) {
            1 -> {
                vincita = if (indovinati == 1) 3 else vincita

            }
            2 -> {
                vincita = if (indovinati == 1) 0 else vincita
                vincita = if (indovinati == 2) 14 else vincita

            }
            3 -> {
                vincita = if (indovinati == 2) 2 else vincita
                vincita = if (indovinati == 3) 50 else vincita

            }
            4 -> {
                vincita = if (indovinati == 2) 1 else vincita
                vincita = if (indovinati == 3) 10 else vincita
                vincita = if (indovinati == 4) 100 else vincita

            }
            5 -> {
                vincita = if (indovinati == 2) 1 else vincita
                vincita = if (indovinati == 3) 4 else vincita
                vincita = if (indovinati == 4) 15 else vincita
                vincita = if (indovinati == 5) 150 else vincita

            }
            6 -> {
                vincita = if (indovinati == 3) 2 else vincita
                vincita = if (indovinati == 4) 10 else vincita
                vincita = if (indovinati == 5) 100 else vincita
                vincita = if (indovinati == 6) 1000 else vincita

            }
            7 -> {
                vincita = if (indovinati == 0) 1 else vincita
                vincita = if (indovinati == 4) 4 else vincita
                vincita = if (indovinati == 5) 40 else vincita
                vincita = if (indovinati == 6) 400 else vincita
                vincita = if (indovinati == 7) 2000 else vincita

            }
            8 -> {
                vincita = if (indovinati == 0) 1 else vincita
                vincita = if (indovinati == 5) 20 else vincita
                vincita = if (indovinati == 6) 200 else vincita
                vincita = if (indovinati == 7) 1000 else vincita
                vincita = if (indovinati == 8) 10000 else vincita

            }
            9 -> {
                vincita = if (indovinati == 0) 2 else vincita
                vincita = if (indovinati == 5) 10 else vincita
                vincita = if (indovinati == 6) 40 else vincita
                vincita = if (indovinati == 7) 400 else vincita
                vincita = if (indovinati == 8) 2000 else vincita
                vincita = if (indovinati == 9) 100000 else vincita

            }
            10 -> {
                vincita = if (indovinati == 0) 2 else vincita
                vincita = if (indovinati == 5) 5 else vincita
                vincita = if (indovinati == 6) 15 else vincita
                vincita = if (indovinati == 7) 150 else vincita
                vincita = if (indovinati == 8) 1000 else vincita
                vincita = if (indovinati == 9) 20000 else vincita
                vincita = if (indovinati == 10) 1000000 else vincita

            }
        }
        giocata.vincita = vincita
        giocata.vincitaTot += vincita
    }
*/

    fun integerArray2Primitive(itemArray: Array<Int>): IntArray {
        val result = IntArray(itemArray.size)
        for (i in itemArray.indices) {
            result[i] = itemArray[i]
        }
        return result
    }

    fun integerList2Primitive(listInteger: List<Int>): IntArray {
        val result = IntArray(listInteger.size)
        for (i in listInteger.indices) {
            result[i] = listInteger[i]
        }
        return result
    }

    fun add2ListNoDuplicate(listTarget: MutableList<Int?>, listSource: List<Int?>): List<Int?> {
        for (item in listSource) {
            if (!listTarget.contains(item)) {
                listTarget.add(item)
            }
        }
        return listTarget
    }

    fun add2ListNoDuplicate(listTarget: MutableList<Int?>, aSource: IntArray): List<Int?> {
        for (item in aSource) {
            if (!listTarget.contains(item)) {
                listTarget.add(item)
            }
        }
        return listTarget
    }

    fun dpToPx(dp: Int): Int {
        // val density = context!!.resources.displayMetrics.densityDpi
        return dp*2
    }

    companion object {
        private var instance : GamesUtil? = null
        fun getInstance(context: Context): GamesUtil {
            if (instance == null) {  // NOT thread safe!
                instance = GamesUtil()
                instance!!.context = context
            }
            return instance!!
        }
    }
}