package it.b4fun.lotto.util.file

import android.content.Context
import android.os.Environment
import android.os.Process
import android.util.Log
import it.b4fun.lotto.util.DateUtils
import java.io.*

class FileManager private constructor(context:Context){
    private val TAG = "FileManager"
    var publicPath:String
    var absPath:String
    //lateinit var _context: Context

    // private var fw: FileWriter? = null
    // private var fOut: File? = null
    // var absPath = Environment.getExternalStorageDirectory().absolutePath + File.separator + "10elottoKtl"
    // var publicPath = Environment.getExternalStorageDirectory().absolutePath + File.separator + "10elottoKtl" + File.separator
    // Init using context argument
    init {
        absPath = context.filesDir.absolutePath + File.separator + "10elottoKtl"
        publicPath = absPath + File.separator
        Log.d(TAG, "Init:$publicPath")
        //_context = context
        val fOut = File(absPath)
        if (!fOut!!.exists()) {
            Log.d(TAG, "crea la directory")
            fOut.mkdirs()
            Log.d(TAG, "directory creata")
        }
    }

    companion object {
        @Volatile
        var instance:FileManager? = null
        @Synchronized
        fun getInstance(context:Context): FileManager = instance?:FileManager(context).also { instance = it }
    }

    fun log2File(message: String, fileName: String) {
        try {
            Log.d(TAG, "path:" + publicPath + fileName)
            val file = File(publicPath, fileName)
            if (!file!!.exists()) {
                Log.d(TAG, "creo il file...")
                file.createNewFile()
                Log.d(TAG, "file  creato...")
            }
            val fileOutputStream = FileOutputStream(file, true)
            val writer = OutputStreamWriter(fileOutputStream)
            writer.append("[PID:${Process.myPid()}]${DateUtils.getDate4log()}$message")
            writer.flush()
            writer.close()
            fileOutputStream.close()
        }
        catch (e: IOException) {
            Log.e(TAG, e.toString())
        }
    }
}