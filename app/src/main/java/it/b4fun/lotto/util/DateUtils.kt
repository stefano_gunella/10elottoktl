package it.b4fun.lotto.util

import android.util.Log
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

// Object dichiara un SINGLETON
object DateUtils {
    private val TAG = "DateUtils"
    var dateFormat = SimpleDateFormat("yyyyMMdd")
    var dateFormat2 = SimpleDateFormat("dd/MM/yyyy")

    fun getNow(): Long {
        val c = Calendar.getInstance()
        return c.timeInMillis
    }

    fun getDate(time: Long): Date {
        val c = Calendar.getInstance()
        c.timeInMillis = time
        return c.time
    }

    fun getPostDelayTime(): Long {
        val c = Calendar.getInstance()
        var mm: Int = c.get(Calendar.MINUTE)
        var ss: Int = c.get(Calendar.SECOND)
        var millisec_5min: Long = ((60 * 5 * 1000 - 60 * (mm % 5) * 1000 - ss * 1000) + 5 * 1000).toLong()
        return millisec_5min
    }

    /**
     * date: data dell'estrazione
     * index: indice estrazione 1-288
     */
    fun getID(date:Date, index:Int): Long {
        var res: Long           // ID estrazione
        var millis_index: Long  // millisecondi
        val c = Calendar.getInstance()
        c.time = date
        c.set(Calendar.MILLISECOND,0)
        c.set(Calendar.SECOND,0)
        c.set(Calendar.HOUR_OF_DAY,0)
        c.set(Calendar.MINUTE,0)
        millis_index = index * 300000L - 1000L
        res = c.timeInMillis + millis_index
        Log.d(TAG, "res : ${res}")
        return res
    }

    fun getNextIndexNow(): Int {
        var res: Int        // da 1 a 288
        var mod_index: Int  // resto da /5
        var hh: Int = 0
        var mm: Int = 0
        val c = Calendar.getInstance()
        hh = c.get(Calendar.HOUR_OF_DAY)
        mm = c.get(Calendar.MINUTE)
        mod_index = (hh * 60 + mm) % 5
        res = if (mod_index == 0) ((hh * 60 + mm) / 5) else ((hh * 60 + mm) / 5 + 1)
        return res
    }

    fun getLastIndexNow(): Int {
        var res: Int = 0
        var index: Int = 0
        var hh: Int = 0
        var mm: Int = 0
        val c = Calendar.getInstance()
        hh = c.get(Calendar.HOUR_OF_DAY)
        mm = c.get(Calendar.MINUTE)
        index = ((hh * 60 + mm) / 5)
        res = if (index == 0) 288 else index
        return res
    }

    fun getYesterday(): Date {
        var dd: Int = 0
        val c = Calendar.getInstance()
        c.add(Calendar.DATE, -1);
        return c.time
    }

    fun getToday(): Date {
        val c = Calendar.getInstance()
        return c.time
    }

    /***
     * Dice che giorno è oggi, ma 30 secondi indietro
     * Questo perchè a mezzanotte l'estrrazione deve essere fatta al giorno precedente
     */
    fun getToday_30secondsAgo(): Date {
        val c = Calendar.getInstance()
        c.add(Calendar.SECOND,-30)
        return c.time
    }

    /*
    fun isToday(day: Calendar): Boolean {
        val today = Calendar.getInstance()
        val d1 = day[Calendar.DAY_OF_YEAR]
        val d2 = today[Calendar.DAY_OF_YEAR]
        return d1 == d2
    }
    */

    fun getDate4log(): String {
        val calendar = Calendar.getInstance()
        val hh = calendar[Calendar.HOUR_OF_DAY]
        val mm = calendar[Calendar.MINUTE]
        val ss = calendar[Calendar.SECOND]
        return String.format("[%02d:%02d:%02d]", hh, mm, ss)
    }

    fun dateToStringFormat(data: Date): String {
        val c = Calendar.getInstance()
        val time = data.time
        c.timeInMillis = time
        return dateFormat.format(c.time)
    }

    fun dateToStringFormat2(data: Date): String {
        val c = Calendar.getInstance()
        val time = data.time
        c.timeInMillis = time
        return dateFormat2.format(c.time)
    }

    fun stringFormatToDate(data: String): Date {
        lateinit var date: Date
        try {
            date = dateFormat.parse(data)
        }
        catch (e: ParseException) {
            Log.d("DateUtil", e.message!!)
        }
        return date
    }

    /*
    fun stringFormat2ToDate(data: String): Date? {
        var date: Date? = null
        try {
            date = dateFormat2.parse(data)
        } catch (e: ParseException) {
            Log.d("DateUtil", e.message!!)
        }
        return date
    }
    */
}