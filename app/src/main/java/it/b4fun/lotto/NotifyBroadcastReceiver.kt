package it.b4fun.lotto

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.media.Ringtone
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.app.NotificationCompat
import it.b4fun.lotto.db.LottoDataItem
import it.b4fun.lotto.util.file.FileManager

open class NotifyBroadcastReceiver : BroadcastReceiver() {
    val TAG: String = "NotifyBroadcastReceiver"
    val notificationId: Int = 101
    val CHANNEL_ID: String = "LOG_CHANNEL"
    lateinit var uri_raw_ding : Uri
    lateinit var uri_raw_coin : Uri
    lateinit var la : LottoApplication
    var item: LottoDataItem? = null

    override fun onReceive(context: Context, intent: Intent) {
        uri_raw_ding = Uri.parse("android.resource://" + context.packageName + "/" + R.raw.ding)
        uri_raw_coin = Uri.parse("android.resource://" + context.packageName + "/" + R.raw.coin)
        var id = intent.getLongExtra("index",0)
        var rightArea = intent.getBooleanExtra("rightArea",false)
        Log.d(TAG, "onReceive index:$id")
        la = context.applicationContext as LottoApplication
        item = la.db.getLotteryDrawById(id)
        var notificationManager : NotificationManager = createNotificationChannel(context)
        StringBuilder().apply {
            append("Action: ${intent.action} ")
            append("Updare rightArea: ${rightArea}\n")
            append("progressivo gg: ${item?.progressivoGiornaliero}\n")
            toString().also { message ->
                la.showMessage(message)
            }
        }
        notifyUpdate(context,notificationManager)
        FileManager.getInstance(context).log2File("progressivo gg: ${item?.progressivoGiornaliero}\n","logBackground.txt")
    }

    private fun createNotificationChannel(context: Context):NotificationManager {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        var aa : AudioAttributes.Builder = AudioAttributes.Builder()
        lateinit var notificationManager: NotificationManager
        with(aa){
            setUsage(AudioAttributes.USAGE_ALARM)
            setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "10 e lotto"
            val descriptionText = "channel_description"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
                setSound(uri_raw_coin,aa.build())
            }
            // Register the channel with the system
            notificationManager = ContextCompat.getSystemService(context, NotificationManager::class.java) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
        return notificationManager
    }

    fun notifyUpdate(context: Context, notificationManager:NotificationManager) : Unit {
        Log.d(TAG, "notifyLog..."+item?._id)
        var notificationBuilder = NotificationCompat.Builder(context, CHANNEL_ID)
            .setSmallIcon(R.drawable.icon_10elotto)
            .setContentTitle("TEST Log")
            .setContentText("prova log:"+item?._id)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
        with(notificationManager) {
            // notificationId is a unique int for each notification that you must define
            notify(notificationId, notificationBuilder.build())
        }
    }
}

