package it.b4fun.lotto.db

import it.b4fun.lotto.util.DateUtils
import java.util.*

/****
 * {"data":20160102,
 * "progressivoGiornaliero":11,
 * "numeriGiocati":["4","6","8","19","22","35","39","43","48","56","62","66","68","69","75","76","77","83","88","89"],
 * "numeroSpeciale":8}
 * @author stefano
 */
class GiocateLottoDataItem {
    var _id: Long = 0
    var id_ext: Long = 0  // indice estrazione
    lateinit var data: Date
    var numeriGiocati = IntArray(10)
    var numeroSpeciale = 0
    var progressivoGiornaliero = 0
    var extStat: Long = 0
    var vincita = 0     // vincita ultima
    var vincitaTot = 0  // vincita totale
    var n_giocate_valide = 0

    val dateUI: String?  get() {
        return DateUtils.dateToStringFormat2(data)
    };

    val totaleNumeriGiocati: Int  get() {
            var result = 0
            for (numero in numeriGiocati) {
                result = if (numero > 0) result + 1 else result
            }
            return result
        }

    fun getNumeriGiocati(): ArrayList<String> {
        Arrays.sort(numeriGiocati)
        val lresult = ArrayList<String>()
        for (numero in numeriGiocati) {
            if (numero != 0) {
                lresult.add("" + numero)
            }
        }
        return lresult
    }

    override fun toString(): String {
        var result: String = ""
        val sb = StringBuilder()
        for (e in numeriGiocati) {
            if (e > 0) {
                sb.append(",$e")
            }
        }
        result += "ID:$_id,id_ext:${id_ext},#NGV:$n_giocate_valide,$data,PG:$progressivoGiornaliero,Ex:${sb.toString().substring(1)},NS:$numeroSpeciale,EXT Stat:${extStat}EXT"
        return result
    }
}