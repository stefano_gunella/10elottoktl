package it.b4fun.lotto.db

import android.content.Context
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

class LottoDbHelper(context: Context?) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    val TAG = "LottoDbHelper"
    override fun onCreate(db: SQLiteDatabase) {
        Log.d(TAG, "onCreate DB: $DATABASE_NAME")
        try {
            db.execSQL(SQL_CREATE_10ELOTTO)
            db.execSQL(SQL_CREATE_GIOCATE10ELOTTO)
        }
        catch (e: SQLException) {
            Log.d(TAG, "ERROR onCreate DB:" + e.message)
        }
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        Log.d(TAG, "onUpgrade - NO DROP TABLES")
        db.execSQL(SQL_DROP_LOTTO)
        db.execSQL(SQL_DROP_GAMES)
        onCreate(db)
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }

    override fun onOpen(db: SQLiteDatabase) {
        super.onOpen(db)
    }

    /***
     * Only for test
     * @param db
     */
    fun onDrop(db: SQLiteDatabase) {
        db.execSQL(SQL_DROP_LOTTO)
        db.execSQL(SQL_DROP_GAMES)
    }

    companion object {
        // If you change the database schema, you must increment the database version.
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "lottoe10.db" // FIXME:questo DB deve essere aggiunto all'applicazione per non caricare i valori dall'inizio.
        private val SQL_CREATE_10ELOTTO =
            "CREATE TABLE " + LottoDataStruct.TABLE_NAME + " (" +
                    LottoDataStruct._ID + " INTEGER PRIMARY KEY," +
                    LottoDataStruct.DATA + " TEXT," +
                    LottoDataStruct._1 + " INTEGER," +
                    LottoDataStruct._2 + " INTEGER," +
                    LottoDataStruct._3 + " INTEGER," +
                    LottoDataStruct._4 + " INTEGER," +
                    LottoDataStruct._5 + " INTEGER," +
                    LottoDataStruct._6 + " INTEGER," +
                    LottoDataStruct._7 + " INTEGER," +
                    LottoDataStruct._8 + " INTEGER," +
                    LottoDataStruct._9 + " INTEGER," +
                    LottoDataStruct._10 + " INTEGER," +
                    LottoDataStruct._11 + " INTEGER," +
                    LottoDataStruct._12 + " INTEGER," +
                    LottoDataStruct._13 + " INTEGER," +
                    LottoDataStruct._14 + " INTEGER," +
                    LottoDataStruct._15 + " INTEGER," +
                    LottoDataStruct._16 + " INTEGER," +
                    LottoDataStruct._17 + " INTEGER," +
                    LottoDataStruct._18 + " INTEGER," +
                    LottoDataStruct._19 + " INTEGER," +
                    LottoDataStruct._20 + " INTEGER," +
                    LottoDataStruct.PG + " INTEGER," +
                    LottoDataStruct.MPG + " INTEGER," +
                    LottoDataStruct.NS + " INTEGER)"
        private val SQL_CREATE_GIOCATE10ELOTTO =
            "CREATE TABLE " + GiocateLottoDataStruct.TABLE_NAME + " (" + GiocateLottoDataStruct._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    GiocateLottoDataStruct.DATA + " TEXT," +
                    GiocateLottoDataStruct.ID_EXT + " INTEGER," +
                    GiocateLottoDataStruct._1 + " INTEGER," +
                    GiocateLottoDataStruct._2 + " INTEGER," +
                    GiocateLottoDataStruct._3 + " INTEGER," +
                    GiocateLottoDataStruct._4 + " INTEGER," +
                    GiocateLottoDataStruct._5 + " INTEGER," +
                    GiocateLottoDataStruct._6 + " INTEGER," +
                    GiocateLottoDataStruct._7 + " INTEGER," +
                    GiocateLottoDataStruct._8 + " INTEGER," +
                    GiocateLottoDataStruct._9 + " INTEGER," +
                    GiocateLottoDataStruct._10 + " INTEGER," +
                    GiocateLottoDataStruct.NS + " INTEGER," +
                    GiocateLottoDataStruct.PG + " INTEGER," +
                    GiocateLottoDataStruct.EXT_STAT + " INTEGER," +
                    GiocateLottoDataStruct.EXT_DELTA + " INTEGER," +
                    GiocateLottoDataStruct.EXT_DIFF + " INTEGER," +
                    GiocateLottoDataStruct.WIN + " INTEGER," +
                    GiocateLottoDataStruct.WIN_TOT + " INTEGER," +
                    GiocateLottoDataStruct.N_GIOCATE_VALIDE + " INTEGER);"
        private const val SQL_DROP_LOTTO = "DROP TABLE IF EXISTS " + LottoDataStruct.TABLE_NAME + ";"
        private const val SQL_DROP_GAMES = "DROP TABLE IF EXISTS " + GiocateLottoDataStruct.TABLE_NAME + ";"
    }
}