package it.b4fun.lotto.db

import android.os.Build.ID
import it.b4fun.lotto.util.DateUtils
import java.util.*

/***
 * {"data":1643497499000,
 * "progressivoGiornaliero":1,
 * "massimoProgressivoGiornaliero":null,
 * "numeriEstratti":["4","6","11","18","24","31","34","38","46","49","51","52","55","62","68","72","76","80","81","87"],
 * "numeriEstrattiOvertime":["12","13","14","15","19","20","22","45","58","73","74","75","79","84","88"],
 * "numeroSpeciale":6,
 * "doppioNumeroSpeciale":87}
 *  @author stefano
 */
class LottoDataItem{
    var _id: Long = 0
    lateinit var data: Date
    var progressivoGiornaliero = 0
    var massimoProgressivoGiornaliero = 0
    var numeriEstratti = IntArray(20)
    var numeriEstrattiOvertime = IntArray(20)
    var numeroSpeciale = 0
    var doppioNumeroSpeciale = 0
    var indexExt: Long = 0

    val dateUI: String  get() {
        return DateUtils.dateToStringFormat2(data)
    }

    val dateDB: String  get() {
        return DateUtils.dateToStringFormat(data)
    }

    override fun toString(): String {
        val sb = StringBuilder()
        for (e in numeriEstratti) {
            sb.append(",$e")
        }
        return "ID:$_id,DATA:$dateDB,PG:$progressivoGiornaliero,Ex:${sb.substring(1)},NS:$numeroSpeciale".trimIndent()
    }
}