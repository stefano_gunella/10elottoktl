package it.b4fun.lotto.db

import android.provider.BaseColumns

/****
 * {"data":yyyyMMdd,
 * "progressivoGiornaliero":11,
 * "massimoProgressivoGiornaliero":null,
 * "numeriEstratti":["4","6","8","19","22","35","39","43","48","56","62","66","68","69","75","76","77","83","88","89"],
 * "numeroSpeciale":8}
 * @author stefano
 */
object LottoDataStruct : BaseColumns {
    const val TABLE_NAME = "_10elotto" //non puo cominciare per un numero.
    const val DATA = "data"
    const val PG = "progressivoGiornaliero"
    const val MPG = "massimoProgressivoGiornaliero"
    const val _1 = "e1"
    const val _2 = "e2"
    const val _3 = "e3"
    const val _4 = "e4"
    const val _5 = "e5"
    const val _6 = "e6"
    const val _7 = "e7"
    const val _8 = "e8"
    const val _9 = "e9"
    const val _10 = "e10"
    const val _11 = "e11"
    const val _12 = "e12"
    const val _13 = "e13"
    const val _14 = "e14"
    const val _15 = "e15"
    const val _16 = "e16"
    const val _17 = "e17"
    const val _18 = "e18"
    const val _19 = "e19"
    const val _20 = "e20"
    const val NS = "numeroSpeciale"
    const val _ID = BaseColumns._ID
    const val _COUNT = BaseColumns._COUNT
}