package it.b4fun.lotto.db

import android.provider.BaseColumns

/****
 * {"data":20160101,
 * "progressivoGiornaliero":11,
 * "numeriGiocati":["1",...,"10"]
 * @author stefano
 */
object GiocateLottoDataStruct : BaseColumns {
    const val TABLE_NAME = "giocate10elotto" // non pu? cominciare per un numero.
    const val DATA = "data"
    const val ID_EXT = "id_ext" // indice estrazione associata alla giocata
    const val PG = "progressivoGiornaliero"
    const val _1 = "e1"
    const val _2 = "e2"
    const val _3 = "e3"
    const val _4 = "e4"
    const val _5 = "e5"
    const val _6 = "e6"
    const val _7 = "e7"
    const val _8 = "e8"
    const val _9 = "e9"
    const val _10 = "e10"
    const val NS = "numeroSpeciale"
    const val EXT_STAT = "ext_stat"     // numero di estrazioni usate per la previsione
    const val EXT_DELTA = "ext_delta"   // delta numero di estrazioni usate per la previsione
    const val EXT_DIFF = "ext_diff"     // delta numero di estrazioni attese per una vincita
    const val WIN = "vincita"           // ultima vincita
    const val WIN_TOT = "vincita_tot"   // vincita Totale
    const val N_GIOCATE_VALIDE = "n_giocate_valide" // numero di volte in cui la giocata ? valida
    const val _ID = BaseColumns._ID
    const val _COUNT = BaseColumns._COUNT
}