package it.b4fun.lotto.db

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.os.Environment
import android.util.Log
import it.b4fun.lotto.util.DateUtils
import it.b4fun.lotto.util.DateUtils.dateToStringFormat
import it.b4fun.lotto.util.file.FileManager
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.OutputStream
import java.util.*

class DbManager(context: Context) {
    private val TAG = "DbManager"
    lateinit var dbhelper: LottoDbHelper
    lateinit var context: Context
    var columnsEstrazioni = arrayOf(
        LottoDataStruct._ID,
        LottoDataStruct.DATA,
        LottoDataStruct.PG,
        LottoDataStruct.MPG,
        LottoDataStruct._1,
        LottoDataStruct._2,
        LottoDataStruct._3,
        LottoDataStruct._4,
        LottoDataStruct._5,
        LottoDataStruct._6,
        LottoDataStruct._7,
        LottoDataStruct._8,
        LottoDataStruct._9,
        LottoDataStruct._10,
        LottoDataStruct._11,
        LottoDataStruct._12,
        LottoDataStruct._13,
        LottoDataStruct._14,
        LottoDataStruct._15,
        LottoDataStruct._16,
        LottoDataStruct._17,
        LottoDataStruct._18,
        LottoDataStruct._19,
        LottoDataStruct._20,
        LottoDataStruct.NS
    )

    init {
        Log.d(TAG, "Init DB")
        this.context = context
        this.dbhelper = LottoDbHelper(context) // inizializza il DB
        this.dbhelper.readableDatabase.isDatabaseIntegrityOk;
    }

    @Synchronized
    fun drop(): Boolean {
        return context!!.deleteDatabase(LottoDbHelper.DATABASE_NAME)
    }

    @Synchronized
    fun close() {
        dbhelper!!.close()
    }

    @Synchronized
    fun insertLotteryDraw(item: LottoDataItem) {
        val db = dbhelper.writableDatabase
        val cv = ContentValues()
        cv.put(LottoDataStruct._ID, item._id)
        cv.put(LottoDataStruct.DATA, item.dateDB)
        cv.put(LottoDataStruct.PG, item.progressivoGiornaliero)
        cv.put(LottoDataStruct.MPG, item.massimoProgressivoGiornaliero)
        cv.put(LottoDataStruct._1, item.numeriEstratti[0])
        cv.put(LottoDataStruct._2, item.numeriEstratti[1])
        cv.put(LottoDataStruct._3, item.numeriEstratti[2])
        cv.put(LottoDataStruct._4, item.numeriEstratti[3])
        cv.put(LottoDataStruct._5, item.numeriEstratti[4])
        cv.put(LottoDataStruct._6, item.numeriEstratti[5])
        cv.put(LottoDataStruct._7, item.numeriEstratti[6])
        cv.put(LottoDataStruct._8, item.numeriEstratti[7])
        cv.put(LottoDataStruct._9, item.numeriEstratti[8])
        cv.put(LottoDataStruct._10, item.numeriEstratti[9])
        cv.put(LottoDataStruct._11, item.numeriEstratti[10])
        cv.put(LottoDataStruct._12, item.numeriEstratti[11])
        cv.put(LottoDataStruct._13, item.numeriEstratti[12])
        cv.put(LottoDataStruct._14, item.numeriEstratti[13])
        cv.put(LottoDataStruct._15, item.numeriEstratti[14])
        cv.put(LottoDataStruct._16, item.numeriEstratti[15])
        cv.put(LottoDataStruct._17, item.numeriEstratti[16])
        cv.put(LottoDataStruct._18, item.numeriEstratti[17])
        cv.put(LottoDataStruct._19, item.numeriEstratti[18])
        cv.put(LottoDataStruct._20, item.numeriEstratti[19])
        cv.put(LottoDataStruct.NS, item.numeroSpeciale)
        try {
            db.insertOrThrow(LottoDataStruct.TABLE_NAME, null, cv)
            Log.d(TAG, "Lottery Draw Inserted...")
        }
        catch (sqle: SQLiteConstraintException) {
            Log.d(TAG, "ATTENZIONE: record già presente" + sqle.message)
        }
        catch (sqle: SQLiteException) {
            Log.d(TAG, "Errore:" + sqle.message)
        }
        finally {
            db.close()
        }
    }

    /***
     * Recupera una certa estrazione ad una certa data
     * lasciare SYNCHRONIZED altrimenti si rompe tutto!!!
     * @param date
     * @param extraction
     * @return
     */
    @Synchronized
    fun getLotteryDraws(date: String, extraction: Int): LottoDataItem? {
        val db = dbhelper!!.readableDatabase
        var result: LottoDataItem? = null
        try {
            val c = db.rawQuery(
                "SELECT * FROM " + LottoDataStruct.TABLE_NAME +
                        " WHERE " + LottoDataStruct.DATA + "=? AND " + LottoDataStruct.PG + "=?",
                arrayOf(date, extraction.toString())
            )
            if (c.moveToFirst()) {
                result = _cursor2LottoDataItem(c)
            }
        } catch (sqle: SQLiteException) {
            Log.d(TAG, "Errore:$sqle")
        } finally {
            db.close()
        }
        return result
    }

    /***
     * Tutte le estrazioni a partire da una data
     * @param date
     * @return
     */
    @Synchronized
    fun loadAllLotteryDrawsFromDate(date: String, extDelta: Int?): List<LottoDataItem> {
        val db = dbhelper!!.readableDatabase
        val lresult: MutableList<LottoDataItem> = ArrayList()
        try {
            val c = db.rawQuery(
                "SELECT * FROM " + LottoDataStruct.TABLE_NAME + " WHERE " + LottoDataStruct.DATA + ">=? AND " + LottoDataStruct.PG + "!=0 ORDER BY " + LottoDataStruct.DATA + " ASC, " + LottoDataStruct.PG + " ASC OFFSET ?",
                arrayOf(date, "extDelta")
            )
            while (c.moveToNext()) {
                lresult.add(_cursor2LottoDataItem(c))
            }
        } catch (sqle: SQLiteException) {
            Log.d(TAG, "Errore:$sqle")
        } finally {
            // Log.d(TAG,"DB_CLOSE:");
            db.close()
        }
        return lresult
    }

    /***
     * Tutte le ultime N estrazioni
     * @param extStat
     * @param extDelta
     * @return
     */
    @Synchronized
    fun loadLastNExtractions(extStat: Int, extDelta: Int): List<LottoDataItem> {
        val db = dbhelper!!.readableDatabase
        val extIndex = DateUtils.getNextIndexNow()
        // Ultima estrazione del giorno...
        val extIndexStart = extIndex - extDelta - extStat
        val extIndexStop = extIndex - extDelta
        val lresult: MutableList<LottoDataItem> = ArrayList()
        try {
            val c = db.rawQuery(
                "SELECT * FROM " + LottoDataStruct.TABLE_NAME +
                        " WHERE " + LottoDataStruct._ID + ">? AND " + LottoDataStruct._ID + "<=?",
                arrayOf("" + extIndexStart, "" + extIndexStop)
            )
            while (c.moveToNext()) {
                lresult.add(_cursor2LottoDataItem(c))
            }
        } catch (sqle: SQLiteException) {
            Log.d(TAG, "Errore:$sqle")
        } finally {
            // Log.d(TAG,"DB_CLOSE:");
            db.close()
        }
        return lresult
    }

    @SuppressLint("Range")
    private fun _cursor2LottoDataItem(c: Cursor): LottoDataItem {
        val item = LottoDataItem()
        item._id = c.getLong(c.getColumnIndex(LottoDataStruct._ID))
        item.data = DateUtils.stringFormatToDate(c.getString(c.getColumnIndex(LottoDataStruct.DATA)))
        item.progressivoGiornaliero = c.getInt(c.getColumnIndex(LottoDataStruct.PG))
        item.massimoProgressivoGiornaliero = c.getInt(c.getColumnIndex(LottoDataStruct.MPG))
        item.numeriEstratti[0] = c.getInt(c.getColumnIndex(LottoDataStruct._1))
        item.numeriEstratti[1] = c.getInt(c.getColumnIndex(LottoDataStruct._2))
        item.numeriEstratti[2] = c.getInt(c.getColumnIndex(LottoDataStruct._3))
        item.numeriEstratti[3] = c.getInt(c.getColumnIndex(LottoDataStruct._4))
        item.numeriEstratti[4] = c.getInt(c.getColumnIndex(LottoDataStruct._5))
        item.numeriEstratti[5] = c.getInt(c.getColumnIndex(LottoDataStruct._6))
        item.numeriEstratti[6] = c.getInt(c.getColumnIndex(LottoDataStruct._7))
        item.numeriEstratti[7] = c.getInt(c.getColumnIndex(LottoDataStruct._8))
        item.numeriEstratti[8] = c.getInt(c.getColumnIndex(LottoDataStruct._9))
        item.numeriEstratti[9] = c.getInt(c.getColumnIndex(LottoDataStruct._10))
        item.numeriEstratti[10] = c.getInt(c.getColumnIndex(LottoDataStruct._11))
        item.numeriEstratti[11] = c.getInt(c.getColumnIndex(LottoDataStruct._12))
        item.numeriEstratti[12] = c.getInt(c.getColumnIndex(LottoDataStruct._13))
        item.numeriEstratti[13] = c.getInt(c.getColumnIndex(LottoDataStruct._14))
        item.numeriEstratti[14] = c.getInt(c.getColumnIndex(LottoDataStruct._15))
        item.numeriEstratti[15] = c.getInt(c.getColumnIndex(LottoDataStruct._16))
        item.numeriEstratti[16] = c.getInt(c.getColumnIndex(LottoDataStruct._17))
        item.numeriEstratti[17] = c.getInt(c.getColumnIndex(LottoDataStruct._18))
        item.numeriEstratti[18] = c.getInt(c.getColumnIndex(LottoDataStruct._19))
        item.numeriEstratti[19] = c.getInt(c.getColumnIndex(LottoDataStruct._20))
        item.numeroSpeciale = c.getInt(c.getColumnIndex(LottoDataStruct.NS))
        return item
    }

    @Synchronized
    fun deleteEstrazione(id: Long): Boolean {
        val db = dbhelper!!.writableDatabase
        return try {
            db.delete(
                LottoDataStruct.TABLE_NAME,
                LottoDataStruct._ID + "=?",
                arrayOf(java.lang.Long.toString(id))
            ) > 0
        } catch (sqle: SQLiteException) {
            false
        } finally {
            // Log.d(TAG,"DB_CLOSE:");
            db.close()
        }
    }

    /***
     * @param id
     * @return
     */
    fun getLotteryDrawById(id: Long): LottoDataItem? {
        var cursor: Cursor? = null
        val whereClause = LottoDataStruct._ID + "=?"
        val whereArgs = arrayOf(id.toString())
        var result: LottoDataItem? = null
        val db = dbhelper!!.readableDatabase
        try {
            cursor = db.query(
                LottoDataStruct.TABLE_NAME,
                columnsEstrazioni,  // The columns to return
                whereClause,  // The columns for the WHERE clause
                whereArgs,  // The values for the WHERE clause
                null,  // don't group the rows
                null,  // don't filter by row groups
                null // don't sort
            )
            if (cursor.moveToFirst()) {
                result = _cursor2LottoDataItem(cursor)
            }
        } catch (ise: IllegalStateException) {
            Log.d(TAG, "Exception:" + ise.message)
            return null
        } catch (sqle: SQLiteException) {
            Log.d(TAG, "Exception:" + sqle.message)
            return null
        } finally {
            db.close()
        }
        return result
    }

    @Deprecated("")
    @Synchronized
    fun getIdLastExtractionAtDate(date: Date?): Int {
        var cursor: Cursor? = null
        val selectionArgs = arrayOf(
            dateToStringFormat(
                date!!
            )
        )
        val db = dbhelper!!.readableDatabase
        try {
            cursor = db.rawQuery(
                "SELECT MAX (" + LottoDataStruct.PG + ") FROM " + LottoDataStruct.TABLE_NAME +
                        " WHERE " + LottoDataStruct.DATA + "=?", selectionArgs
            )
            cursor.moveToFirst()
        } catch (sqle: SQLiteException) {
            return -1
        } finally {
            // Log.d(TAG,"DB_CLOSE:");
            db.close()
        }
        return cursor!!.getInt(0)
    }

    fun loadAllLotteryDrawsAtDate(date: String): ArrayList<LottoDataItem> {
        val db = dbhelper.readableDatabase
        val lresult: ArrayList<LottoDataItem> = arrayListOf()
        try {
            val cursor = db.rawQuery(
                "SELECT * FROM " + LottoDataStruct.TABLE_NAME + " WHERE " + LottoDataStruct.DATA + "=? ORDER BY " + LottoDataStruct.DATA + " ASC, " + LottoDataStruct._ID + " ASC",
                arrayOf(date, "extDelta")
            )
            while (cursor.moveToNext()) {
                lresult.add(_cursor2LottoDataItem(cursor))
            }
        } catch (sqle: SQLiteException) {
            Log.d(TAG, "Errore:$sqle")
        } finally {
            // Log.d(TAG,"DB_CLOSE:");
            db.close()
        }
        return lresult
    }

    @Synchronized
    fun getListDataDrawsAtDate(date: Date): ArrayList<LottoDataItem>? {
        val lresult: ArrayList<LottoDataItem> = arrayListOf()
        val db = dbhelper.readableDatabase
        val date_string = DateUtils.dateToStringFormat(date)
        try {
            val cursor = db.rawQuery(
                "SELECT * FROM " + LottoDataStruct.TABLE_NAME + " WHERE " + LottoDataStruct.DATA + "=? ORDER BY " + LottoDataStruct._ID + " ASC",
                arrayOf(date_string)
            )
            while (cursor.moveToNext()) {
                lresult.add(_cursor2LottoDataItem(cursor))
            }
        }
        catch (sqle: SQLiteException) {
            return null
        }
        finally {
            // Log.d(TAG,"DB_CLOSE:");
            db.close()
        }
        return lresult
    }

    /***
     * Restituisce tutte le estrazioni comprese tra le due selezionate
     * @param index1 prima estrazione
     * @param index2 seconda estrazione
     * @return
     */
    @Synchronized
    fun getListDataDrawsFromEx1Ex2(index1: Long, index2: Long): ArrayList<LottoDataItem>? {
        val lresult: ArrayList<LottoDataItem> = arrayListOf()
        val db = dbhelper.readableDatabase
        try {
            val cursor = db.rawQuery(
                "SELECT * FROM " + LottoDataStruct.TABLE_NAME + " WHERE " + LottoDataStruct._ID + ">=? AND " + LottoDataStruct._ID + "<=? ORDER BY " + LottoDataStruct._ID + " ASC",
                arrayOf("" + index1, "" + index2)
            )
            while (cursor.moveToNext()) {
                lresult.add(_cursor2LottoDataItem(cursor))
            }
        }
        catch (sqle: SQLiteException) {
            return null
        }
        finally {
            // Log.d(TAG,"DB_CLOSE:");
            db.close()
        }
        return lresult
    }
    /*
	================
	GESTIONE GIOCATE
	================
	*/
    /***
     * Una giocata puo' contenere da 1 a 10 numeri.
     * Tutte le giocate inserite vengono calcolate valide nel calcolo dell'importo della giocata.
     * Non si possono inserire 2 giocate con gli stessi numeri.
     * Non viene mantenuto lo storico.
     * @param item
     */
    @Synchronized
    fun saveGiocate(item: GiocateLottoDataItem) {
        val lGames = getListGiocateBy(item) // vediamo se esiste gia' una giocta uguale
        if (lGames.size > 0) {
            Log.d(TAG, "saveGiocate - GIOCATA DOPPIA:$item")
            return
        }
        val db = dbhelper.writableDatabase
        val cv = ContentValues()
        cv.put(GiocateLottoDataStruct.DATA, DateUtils.dateToStringFormat(item.data))
        cv.put(GiocateLottoDataStruct.ID_EXT, item.id_ext)
        cv.put(GiocateLottoDataStruct.PG, item.progressivoGiornaliero)
        cv.put(GiocateLottoDataStruct._1, item.numeriGiocati[0])
        cv.put(GiocateLottoDataStruct._2, item.numeriGiocati[1])
        cv.put(GiocateLottoDataStruct._3, item.numeriGiocati[2])
        cv.put(GiocateLottoDataStruct._4, item.numeriGiocati[3])
        cv.put(GiocateLottoDataStruct._5, item.numeriGiocati[4])
        cv.put(GiocateLottoDataStruct._6, item.numeriGiocati[5])
        cv.put(GiocateLottoDataStruct._7, item.numeriGiocati[6])
        cv.put(GiocateLottoDataStruct._8, item.numeriGiocati[7])
        cv.put(GiocateLottoDataStruct._9, item.numeriGiocati[8])
        cv.put(GiocateLottoDataStruct._10, item.numeriGiocati[9])
        cv.put(GiocateLottoDataStruct.NS, item.numeroSpeciale)
        cv.put(GiocateLottoDataStruct.EXT_STAT, item.extStat)
        cv.put(GiocateLottoDataStruct.WIN, item.vincita)
        cv.put(GiocateLottoDataStruct.WIN_TOT, item.vincitaTot)
        cv.put(GiocateLottoDataStruct.N_GIOCATE_VALIDE, item.n_giocate_valide)
        try {
            Log.d(TAG, "GIOCATA:$item")
            db.insert(GiocateLottoDataStruct.TABLE_NAME, null, cv)
        } catch (sqle: SQLiteException) {
            Log.d(TAG, "saveGiocate Errore:$sqle")
        } finally {
            // Log.d(TAG,"DB_CLOSE:");
            db.close()
        }
    }

    /***
     * Recupera tutte le giocate con gli stessi numeri di item.
     * @param item
     * @return
     */
    @Synchronized
    fun getListGiocateBy(item: GiocateLottoDataItem): List<GiocateLottoDataItem> {
        val db = dbhelper.readableDatabase
        val lresult: MutableList<GiocateLottoDataItem> = ArrayList()
        val sNumeriGiocati = arrayOfNulls<String>(10)
        try {
            for (i in 0..9) {
                sNumeriGiocati[i] =
                    if (item.numeriGiocati[i] == 0) "0" else "" + item.numeriGiocati[i]
            }
            val c = db.rawQuery(
                "SELECT * FROM " + GiocateLottoDataStruct.TABLE_NAME + " WHERE " +
                        GiocateLottoDataStruct._1 + "=? AND " +
                        GiocateLottoDataStruct._2 + "=? AND " +
                        GiocateLottoDataStruct._3 + "=? AND " +
                        GiocateLottoDataStruct._4 + "=? AND " +
                        GiocateLottoDataStruct._5 + "=? AND " +
                        GiocateLottoDataStruct._6 + "=? AND " +
                        GiocateLottoDataStruct._7 + "=? AND " +
                        GiocateLottoDataStruct._8 + "=? AND " +
                        GiocateLottoDataStruct._9 + "=? AND " +
                        GiocateLottoDataStruct._10 + "=? ",
                sNumeriGiocati
            )
            while (c.moveToNext()) {
                lresult.add(_cursor2GiocataLottoDataItem(c))
            }
        } catch (sqle: SQLiteException) {
            Log.d(TAG, "Errore:$sqle")
        } finally {
            // Log.d(TAG,"saveGiocate - DB_CLOSE:");
            db.close()
        }
        return lresult
    }

    // Log.d(TAG,"getListGiocate - DB_CLOSE:");
    @get:Synchronized
    val listGiocate: List<GiocateLottoDataItem>
        get() {
            val db = dbhelper!!.readableDatabase
            val lresult: MutableList<GiocateLottoDataItem> = ArrayList()
            try {
                val c = db.rawQuery(
                    "SELECT * FROM " + GiocateLottoDataStruct.TABLE_NAME,
                    null
                )
                while (c.moveToNext()) {
                    lresult.add(_cursor2GiocataLottoDataItem(c))
                }
            } catch (sqle: SQLiteException) {
                Log.d(TAG, "Errore:$sqle")
            } finally {
                // Log.d(TAG,"getListGiocate - DB_CLOSE:");
                db.close()
            }
            return lresult
        }

    @Synchronized
    fun getGiocataById(id: Int): GiocateLottoDataItem? {
        val db = dbhelper!!.readableDatabase
        var result: GiocateLottoDataItem? = null
        try {
            val c = db.rawQuery(
                "SELECT * FROM " + GiocateLottoDataStruct.TABLE_NAME +
                        " WHERE " + GiocateLottoDataStruct._ID + "=?", arrayOf("" + id)
            )
            if (c.moveToFirst()) {
                result = _cursor2GiocataLottoDataItem(c)
            }
        } catch (sqle: SQLiteException) {
            Log.d(TAG, "Errore:$sqle")
        } finally {
            // Log.d(TAG,"DB_CLOSE:");
            db.close()
        }
        return result
    }

    @Synchronized
    fun updateVincita(item: GiocateLottoDataItem): Int {
        var result = 0
        val db = dbhelper!!.writableDatabase
        val values = ContentValues()
        values.put(GiocateLottoDataStruct.WIN, item.vincita)
        values.put(GiocateLottoDataStruct.WIN_TOT, item.vincitaTot)
        val whereClause = GiocateLottoDataStruct._ID + "=?"
        val whereArgs = arrayOf(item._id.toString() + "")
        try {
            result = db.update(GiocateLottoDataStruct.TABLE_NAME, values, whereClause, whereArgs)
        } catch (sqle: SQLiteException) {
            Log.d(TAG, "saveGiocate Errore:$sqle")
        } finally {
            // Log.d(TAG,"DB_CLOSE:");
            db.close()
        }
        return result
    }

    @Synchronized
    fun deleteAllGames() {
        val db = dbhelper!!.writableDatabase
        db.delete(GiocateLottoDataStruct.TABLE_NAME, null, null)
    }

    @Synchronized
    fun deleteGame(id: Long): Boolean {
        val db = dbhelper!!.writableDatabase
        var result = false
        result = try {
            val n = db.delete(
                GiocateLottoDataStruct.TABLE_NAME,
                GiocateLottoDataStruct._ID + "=?",
                arrayOf(java.lang.Long.toString(id))
            )
            n > 0
        } catch (sqle: SQLiteException) {
            false
        } finally {
            // Log.d(TAG,"DB_CLOSE:");
            db.close()
        }
        return result
    }

    private fun _cursor2GiocataLottoDataItem(c: Cursor): GiocateLottoDataItem {
        val item = GiocateLottoDataItem()
        /*
        item._id = c.getLong(c.getColumnIndex(GiocateLottoDataStruct._ID))
        item.data = DateUtils.stringFormatToDate(c.getString(c.getColumnIndex(GiocateLottoDataStruct.DATA)))
        item.id_ext = c.getLong(c.getColumnIndex(GiocateLottoDataStruct.ID_EXT))
        item.progressivoGiornaliero = c.getInt(c.getColumnIndex(GiocateLottoDataStruct.PG))
        item.numeriGiocati[0] = c.getInt(c.getColumnIndex(GiocateLottoDataStruct._1))
        item.numeriGiocati[1] = c.getInt(c.getColumnIndex(GiocateLottoDataStruct._2))
        item.numeriGiocati[2] = c.getInt(c.getColumnIndex(GiocateLottoDataStruct._3))
        item.numeriGiocati[3] = c.getInt(c.getColumnIndex(GiocateLottoDataStruct._4))
        item.numeriGiocati[4] = c.getInt(c.getColumnIndex(GiocateLottoDataStruct._5))
        item.numeriGiocati[5] = c.getInt(c.getColumnIndex(GiocateLottoDataStruct._6))
        item.numeriGiocati[6] = c.getInt(c.getColumnIndex(GiocateLottoDataStruct._7))
        item.numeriGiocati[7] = c.getInt(c.getColumnIndex(GiocateLottoDataStruct._8))
        item.numeriGiocati[8] = c.getInt(c.getColumnIndex(GiocateLottoDataStruct._9))
        item.numeriGiocati[9] = c.getInt(c.getColumnIndex(GiocateLottoDataStruct._10))
        item.numeroSpeciale = c.getInt(c.getColumnIndex(GiocateLottoDataStruct.NS))
        item.extStat = c.getLong(c.getColumnIndex(GiocateLottoDataStruct.EXT_STAT))
        item.vincita = c.getInt(c.getColumnIndex(GiocateLottoDataStruct.WIN))
        item.vincitaTot = c.getInt(c.getColumnIndex(GiocateLottoDataStruct.WIN_TOT))
        item.n_giocate_valide = c.getInt(c.getColumnIndex(GiocateLottoDataStruct.N_GIOCATE_VALIDE))
    */
        return item
    }
}