package it.b4fun.lotto

import android.R.attr.data
import android.app.Application
import android.content.Intent
import android.util.Log
import android.widget.Toast
import it.b4fun.lotto.db.DbManager
import it.b4fun.lotto.db.LottoDataItem
import it.b4fun.lotto.engine.PostTask
import it.b4fun.lotto.util.DateUtils
import kotlinx.coroutines.*
import java.time.*
import java.time.temporal.Temporal
import java.util.*

class LottoApplication: Application() {
    val TAG = "LottoApplication"
    lateinit var db: DbManager

    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "Start Application...")
        this.db = DbManager(applicationContext)
        // initialize Rudder SDK here
    }

    fun doWebTask(date:Date,index: Int) = runBlocking {
        launch {
            Log.d(TAG, "doWebTask:$index")
            // TODO - db.getLotteryDrawById()
            val ID = DateUtils.getID(date,index)
            Log.d(TAG, "ID:$ID")
            val itemOnDB = db.getLotteryDrawById(ID)
            if (itemOnDB != null) {
                Log.d(TAG, "Exist ID:$ID")
                return@launch
            }
            val task: PostTask = PostTask(applicationContext)
            val item = task.getExtraction(date,index)
            if (item != null) {
                db.insertLotteryDraw(item)
                notifyBroadcast(item)
                updateUIBroadcast(item)
            }
        }
    }

    suspend fun doWebLoadDataTask(fromDate: Date, toDate: Date?) {
        Log.d(TAG, "doWebLoadDataTask")
        val task: PostTask = PostTask(applicationContext)
        // TODO:fromDate - toDate
        var endDay = Date(toDate?.time ?: fromDate.time) // Elvis operator
        // var day1 = Calendar.getInstance().apply { this.time = fromDate }
        // var day2 = Calendar.getInstance().apply { this.time = toDate }
        var day1 : Instant = fromDate.toInstant()
        var day2 : Instant = endDay.toInstant()

        Log.d(TAG, "day difference:") // TODO

        for (index in 1..DateUtils.getLastIndexNow()) {
            val ID = DateUtils.getID(fromDate,index)
            val itemOnDB = db.getLotteryDrawById(ID)
            if (itemOnDB != null) {
                Log.d(TAG, "Exist ID:$ID")
                continue
            }
            val item = task.getExtraction(fromDate, index)
            if (item != null) {
                db.insertLotteryDraw(item)
            }
        }
        updateUIBroadcast(null)
        Log.d(TAG, "END doWebLoadDataTask")
    }

    /*
    Aggiorna la parte DX della prima pagina.
    */
    var isCheckedItem: MutableList<Boolean> = MutableList(288) { false }
    var numbersDrawn = IntArray(90) { 0 }
    fun updateListNumbers(flgAdd:Boolean, item: LottoDataItem? ){
        Log.d(TAG, "updateListNumbers")
        item?:run {
            numbersDrawn.forEachIndexed { index, item -> numbersDrawn[index] = 0 }
            updateUIBroadcast(null)
            return;
        }
        item.numeriEstratti.forEach {
            if(flgAdd){
                numbersDrawn[it-1]++
            }
            else {
                numbersDrawn[it-1]--
                if (numbersDrawn[it-1] < 0) {
                    numbersDrawn[it-1] = 0
                }
            }
        } // incrementa il numero estratto e aggiorna il colore
        updateUI_RightArea_Broadcast()
    }

    fun notifyBroadcast(item: LottoDataItem) {
        Log.d(TAG, "sendBroadcast for Notify...")
        var notifyIntent = Intent(applicationContext, NotifyBroadcastReceiver::class.java)
        notifyIntent.also { i ->
            i.setAction("it.b4fun.lotto.NotifyUpdate")
            i.putExtra("index", item._id)
        }
        sendBroadcast(notifyIntent)
    }

    fun updateUIBroadcast(item: LottoDataItem?) {
        Log.d(TAG, "sendBroadcast to update UI...")
        var updateUIIntent = Intent()
        updateUIIntent.also { i ->
            i.setAction("it.b4fun.lotto.updateUI")
            i.putExtra("index", item?._id)
        }
        sendBroadcast(updateUIIntent)
    }

    fun updateUI_RightArea_Broadcast() {
        Log.d(TAG, "sendBroadcast to update Right Area UI...")
        var updateUIIntent = Intent()
        updateUIIntent.also { i ->
            i.setAction("it.b4fun.lotto.updateUI")
            i.putExtra("rightArea", true)
        }
        sendBroadcast(updateUIIntent)
    }

    fun showMessage(message:String){
        Toast.makeText(this.applicationContext, message, Toast.LENGTH_LONG).show()
    }
}